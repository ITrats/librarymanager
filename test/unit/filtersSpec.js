/// <reference path="../../app/js/Resources.js" />
/// <reference path="../../node_modules/karma-jasmine/lib/jasmine.js" />
"use strict";

/* jasmine specs for filters go here */

describe("filter", function () {
    beforeEach(module(ModuleNames.libraryFilters));

    //unit tests for localSearch
    describe("Local search Filter Test", function () {
        //unit test with no entered search text
        it("should get array of elements and NO filter text and return the same array",
            inject(function (localSearchFilter) {
                var testArr = [];
                var testText;
                var expectedResult = [];
                expect(localSearchFilter(testArr, testText)).toBe(testArr);

                testArr = ["testResult"];
                testText;
                expect(localSearchFilter(testArr, testText)).toBe(testArr);

                testArr = [];
                testText = "";
                expect(localSearchFilter(testArr, testText)).toBe(testArr);

                testArr = ["testResult"];
                testText = "";
                expect(localSearchFilter(testArr, testText)).toBe(testArr);
            }));

        //unit tests for localSearch - search for users
        it("should get string for search and find all people, with name, containing this string",
            inject(function (localSearchFilter) {
                var testArr = [
                  {
                      "firstName": "John",
                      "lastName": "Doe",
                      "username": "tes"
                  },
                  {
                      "firstName": "Jack",
                      "lastName": "Sparrow",
                      "username": "tes"
                  },
                  {
                      "firstName": "Captain",
                      "lastName": "America",
                      "username": "tes"
                  },
                  {
                      "firstName": "Joseph",
                      "lastName": "Sparrow",
                      "username": "tes"
                  },
                  {
                      "firstName": "Joseph",
                      "lastName": "Stalin",
                      "username": "tes"
                  }
                ];
                var testText = "john";
                var expectedResult = [{
                    "firstName": "John",
                    "lastName": "Doe",
                    "username": "tes"
                }];

                expect(localSearchFilter(testArr, testText)).toEqual(expectedResult);

                testText = "Joseph";
                expectedResult = [{
                    "firstName": "Joseph",
                    "lastName": "Sparrow",
                    "username": "tes"
                }, {
                    "firstName": "Joseph",
                    "lastName": "Stalin",
                    "username": "tes"
                }];
                expect(localSearchFilter(testArr, testText)).toEqual(expectedResult);

                testText = "";
                expect(localSearchFilter(testArr, testText)).toEqual(testArr);

                testText = "J";
                expectedResult = [
                  {
                      "firstName": "John",
                      "lastName": "Doe",
                      "username": "tes"
                  },
                  {
                      "firstName": "Jack",
                      "lastName": "Sparrow",
                      "username": "tes"
                  },
                  {
                      "firstName": "Joseph",
                      "lastName": "Sparrow",
                      "username": "tes"
                  },
                  {
                      "firstName": "Joseph",
                      "lastName": "Stalin",
                      "username": "tes"
                  }];
                expect(localSearchFilter(testArr, testText)).toEqual(expectedResult);

                testText = "j";
                expect(localSearchFilter(testArr, testText)).toEqual(expectedResult);

                testText = "L";
                expectedResult = [
                  {
                      "firstName": "Joseph",
                      "lastName": "Stalin",
                      "username": "tes"
                  }];
                expect(localSearchFilter(testArr, testText)).toEqual(expectedResult);
            }));

        //unit tests for localSearch - search for books by title 
        it("should get string for search and find all books with title, containing this string",
            inject(function (localSearchFilter) {
                var testArray = [{
                    "status": "isReadBook",
                    "genre": "Romance",
                    "title": "World of Warcraft - Rise of the Horde",
                    "author": "Christie Golden"
                },
                {
                    "status": "isReadBook",
                    "title": "Warcraft: Of Blood and Honor",
                    "author": "Chris Metzen",
                    "genre": "Horror"
                },
                {
                    "status": "isReadBook",
                    "title": "Warcraft: War of the Ancients #1: The Well of Eternity",
                    "author": "Richard A. Knaak",
                    "genre": "Fiction"
                }];
                var searchString = "WARCRAFT";
                expect(localSearchFilter(testArray, searchString)).toEqual(testArray);

                searchString = "hOnoR";
                var expectedArray = [{
                    "status": "isReadBook",
                    "title": "Warcraft: Of Blood and Honor",
                    "author": "Chris Metzen",
                    "genre": "Horror"
                }];
                expect(localSearchFilter(testArray, searchString)).toEqual(expectedArray);

                searchString = "Warcraft:";
                expectedArray = [{
                    "status": "isReadBook",
                    "title": "Warcraft: Of Blood and Honor",
                    "author": "Chris Metzen",
                    "genre": "Horror"
                },
                {
                    "status": "isReadBook",
                    "title": "Warcraft: War of the Ancients #1: The Well of Eternity",
                    "author": "Richard A. Knaak",
                    "genre": "Fiction"
                }];
                expect(localSearchFilter(testArray, searchString)).toEqual(expectedArray);




                searchString = "Chris Metzen";
                var expectedArray = [{
                    "status": "isReadBook",
                    "title": "Warcraft: Of Blood and Honor",
                    "author": "Chris Metzen",
                    "genre": "Horror"
                }];
                expect(localSearchFilter(testArray, searchString)).toEqual(expectedArray);

                searchString = "asddw";
                expectedArray = [];
                expect(localSearchFilter(testArray, searchString)).toEqual(expectedArray);

                searchString = "no existence";
                expect(localSearchFilter(testArray, searchString)).toEqual([]);
            }));
    });

    //unit tests for sortBooksByStatus
    describe("sortBooksByStatus Tests", function () {
        it("should get book list and sort it by book read status",
           inject(function (sortBooksByStatusFilter) {
               var testBooks = [
                    {
                        "status": "isReadBook",
                        "title": "Warcraft: Of Blood and Honor"
                    },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft: War of the Ancients #1: The Well of Eternity"
                   },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft"
                   },
                   {
                       "status": "toBeRead",
                       "title": "Test heading"
                   },
                   {
                       "status": "currentlyReading",
                       "title": "Some random title"
                   }];

               var expectedStatus = "isReadBook";
               var expectedResult = [
                   {
                       "status": "isReadBook",
                       "title": "Warcraft: Of Blood and Honor"
                   },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft: War of the Ancients #1: The Well of Eternity"
                   },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft"
                   }];
               expect(sortBooksByStatusFilter(testBooks, expectedStatus)).toEqual(expectedResult);

               expectedStatus = "currentlyReading";
               expectedResult = [{
                   "status": "currentlyReading",
                   "title": "Some random title"
               }];
               expect(sortBooksByStatusFilter(testBooks, expectedStatus)).toEqual(expectedResult);

               expectedStatus = "toBeRead";
               expectedResult = [{
                   "status": "toBeRead",
                   "title": "Test heading"
               }];
               expect(sortBooksByStatusFilter(testBooks, expectedStatus)).toEqual(expectedResult);

               expectedStatus = "";
               expect(sortBooksByStatusFilter(testBooks, expectedStatus)).toEqual([]);

               expectedStatus = "wrongStatus";
               expect(sortBooksByStatusFilter(testBooks, expectedStatus)).toEqual([]);
           }));
    });

    //unit tests for advancedLocalSearch
    describe("advancedLocalSearch Tests", function () {
        it("test with not real data",
            inject(function (advancedLocalSearchFilter) {
                var testArray = [];
                var filters = {};
                var result = [];
                expect(advancedLocalSearchFilter(testArray, filters)).toEqual(result);

                testArray = ["aa", "bb", "cc"];
                filters = {};
                result = ["aa", "bb", "cc"];
                expect(advancedLocalSearchFilter(testArray, filters)).toEqual(result);

                testArray = ["aa", "bb", "cc"];
                filters = "as";
                result = ["aa", "bb", "cc"];
                expect(advancedLocalSearchFilter(testArray, filters)).toEqual(result);
            }));

        it("should get book list and genres, titleString, autorString, status and readDate(if status is read) and show books mathing theese filters",
           inject(function (advancedLocalSearchFilter) {
               var testBooks = [
                    {
                        "status": "isReadBook",
                        "title": "Warcraft: Of Blood and Honor",
                        "genre": "Fantasy"
                    },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft: War of the Ancients #1: The Well of Eternity",
                       "genre": "Horror"
                   },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft",
                       "genre": "Fiction"
                   },
                   {
                       "status": "toBeRead",
                       "title": "Test heading",
                       "genre": "Fiction"
                   },
                   {
                       "status": "currentlyReading",
                       "title": "Some random title",
                       "genre": "Romance"
                   }];

               var filters = {
                   title: "",
                   genres: []
               };
               //test with no filters
               expect(advancedLocalSearchFilter(testBooks, filters)).toEqual(testBooks);

               filters = {
                   title: "Warcraft",
                   genres: []
               };
               var expectedResult = [{
                   "status": "isReadBook",
                   "title": "Warcraft: Of Blood and Honor",
                   "genre": "Fantasy"
               },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft: War of the Ancients #1: The Well of Eternity",
                       "genre": "Horror"
                   },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft",
                       "genre": "Fiction"
                   }];
               //test with filters title and two genres
               expect(advancedLocalSearchFilter(testBooks, filters)).toEqual(expectedResult);

               filters = {
                   title: "Warcraft",
                   genres: ["Fantasy", "Horror"]
               };
               var expectedResult = [{
                   "status": "isReadBook",
                   "title": "Warcraft: Of Blood and Honor",
                   "genre": "Fantasy"
               },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft: War of the Ancients #1: The Well of Eternity",
                       "genre": "Horror"
                   }
               ];
               //test with filter only genres
               expect(advancedLocalSearchFilter(testBooks, filters)).toEqual(expectedResult);

               filters = {
                   title: "",
                   genres: ["Fiction", "Horror"]
               };
               var expectedResult = [{
                   "status": "isReadBook",
                   "title": "Warcraft: War of the Ancients #1: The Well of Eternity",
                   "genre": "Horror",
               },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft",
                       "genre": "Fiction",
                   },
                   {
                       "status": "toBeRead",
                       "title": "Test heading",
                       "genre": "Fiction",
                   }];
               //test with filter only heading
               expect(advancedLocalSearchFilter(testBooks, filters)).toEqual(expectedResult);

               filters = {
                   title: "Warcraft",
                   genres: ["Adventure"]
               };
               var expectedResult = [];
               //test with filter: match title, no such a genre
               expect(advancedLocalSearchFilter(testBooks, filters)).toEqual(expectedResult);

               filters = {
                   title: "NotHere",
                   genres: ["Fantasy", "Horror"]
               };
               var expectedResult = [];
               //test with filter genre and no existing title
               expect(advancedLocalSearchFilter(testBooks, filters)).toEqual(expectedResult);
           }));
    });
});
