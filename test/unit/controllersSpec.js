/// <reference path="../../app/bower_components/angular-mocks/angular-mocks.js" />
/// <reference path="../../app/bower_components/angular-resource/angular-resource.js" />
/// <reference path="../../app/bower_components/angular-route/angular-route.js" />
/// <reference path="../../app/js/Resources.js" />
'use strict';

/* jasmine specs for controllers go here */

describe("Controllers Test", function () {

    beforeEach(function () {
        this.addMatchers({
            toEqualData: function (expected) {
                return angular.equals(this.actual, expected);
            }
        });
    });

    beforeEach(module(ModuleNames.libraryControllers));
    beforeEach(module(ModuleNames.libraryAppServices));

    //tests for book details controller
    describe('BookDetailsCtrl', function () {
        var testBook = {
            "status": "isReadBook",
            "title": "Warcraft: Of Blood and Honor"
        };

        var scope, $httpBackend, ctrl, rootScope;

        beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
            $httpBackend = _$httpBackend_;
            rootScope = $rootScope;
            ctrl = $controller;
            scope = rootScope.$new();

            ctrl = ctrl(CtrlNames.BookDetailsCtrl, {
                $scope: scope,
                $routeParams: {
                    bookId: "123"
                }
            });
            $httpBackend.expectGET('json/books/123.json').respond(testBook);
        }));

        it('should get book detail object', function () {
            expect(scope.book).toEqualData({});
            $httpBackend.flush();

            expect(scope.book).toEqualData(testBook);
        });
    });


    //tests for home controller
    describe('HomeCtrl', function () {
        var testBooks = [
                    {
                        "status": "isReadBook",
                        "title": "Warcraft: Of Blood and Honor"
                    },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft: War of the Ancients #1: The Well of Eternity"
                   },
                   {
                       "status": "isReadBook",
                       "title": "Warcraft"
                   },
                   {
                       "status": "toBeRead",
                       "title": "Test heading"
                   },
                   {
                       "status": "currentlyReading",
                       "title": "Some random title"
                   }];
        var scope, $httpBackend, ctrl, rootScope;

        beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
            $httpBackend = _$httpBackend_;
            rootScope = $rootScope;
            ctrl = $controller;
        }));

        it('should be on home page and fetch top 10 books. Should be logged in', function () {
            scope = rootScope.$new();
            rootScope.isHome = function () {
                return true;
            };

            $httpBackend.expectGET('json/books.json?top-picks=10').respond(testBooks);
            expect($httpBackend.flush).toThrow("No pending request to flush !");

            sessionStorage.getItem = function () {
                return "12312";
            };

            ctrl = ctrl(CtrlNames.HomeCtrl, {
                $scope: scope
            });


            expect(scope.books).toEqualData([]);
            $httpBackend.flush();

            expect(scope.books).toEqualData(testBooks);
        });

        it('should not be on homepage and not to fetch anything', function () {
            scope = rootScope.$new();
            rootScope.isHome = function () {
                return false;
            }
            sessionStorage.getItem = function () {
                return "12312";
            };
            ctrl = ctrl(CtrlNames.HomeCtrl, {
                $scope: scope
            });

            $httpBackend.expectGET('json/books.json?top-picks=10"').respond(testBooks);

            expect(scope.books).toBeUndefined();
            expect($httpBackend.flush).toThrow("No pending request to flush !");
            expect(scope.books).toBeUndefined();
        });
    });

    //tests for related profiles controller - looking from my profile
    describe('RelatedProfilesCtrl', function () {
        var scope, $httpBackend, ctrl, rootScope;

        beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
            $httpBackend = _$httpBackend_;
            rootScope = $rootScope;
            ctrl = $controller;
        }));

        it('should show his related users, becouse i am following him', function () {
            sessionStorage.getItem = function () {
                return "12312";
            };
            scope = rootScope.$new();
            scope.user = {
                followed: true
            };
            ctrl = ctrl(CtrlNames.RelatedProfilesCtrl, {
                $scope: scope, $routeParams: {
                    userList: "related"
                }
            });

            $httpBackend.expectGET('json/users.json?userList=related').respond([{
                "userName": "adich",
                "name": "Adolf Hitler",
                "profilePicture": "http://i.telegraph.co.uk/multimedia/archive/01881/hitler_1881083c.jpg",
                "followed": true
            }]);

            expect(scope.users).toEqualData([]);
            $httpBackend.flush();

            expect(scope.users).toEqualData([{
                "userName": "adich",
                "name": "Adolf Hitler",
                "profilePicture": "http://i.telegraph.co.uk/multimedia/archive/01881/hitler_1881083c.jpg",
                "followed": true
            }]);
        });

        it('should show my related users, becouse i am in my profile', function () {
            scope = rootScope.$new();
            scope.user = {
                followed: undefined
            };
            ctrl = ctrl(CtrlNames.RelatedProfilesCtrl, {
                $scope: scope, $routeParams: {
                    userList: "related"
                }
            });
            sessionStorage.getItem = function () {
                return "12312";
            };

            $httpBackend.expectGET('json/users.json?userList=related').respond([{
                "userName": "adich",
                "name": "Adolf Hitler",
                "profilePicture": "http://i.telegraph.co.uk/multimedia/archive/01881/hitler_1881083c.jpg",
                "followed": true
            }]);

            expect(scope.users).toEqualData([]);
            $httpBackend.flush();

            expect(scope.users).toEqualData([{
                "userName": "adich",
                "name": "Adolf Hitler",
                "profilePicture": "http://i.telegraph.co.uk/multimedia/archive/01881/hitler_1881083c.jpg",
                "followed": true
            }]);
        });


        it('should NOT show his related users, becouse i am NOT following him', function () {
            scope = rootScope.$new();
            scope.user = {
                followed: false
            };
            ctrl = ctrl(CtrlNames.RelatedProfilesCtrl, {
                $scope: scope, $routeParams: {
                    userList: "related"
                }
            });
            sessionStorage.getItem = function () {
                return "12312";
            };
            $httpBackend.expectGET('json/users.json?userList=related').respond([{
                "userName": "adich",
                "name": "Adolf Hitler",
                "profilePicture": "http://i.telegraph.co.uk/multimedia/archive/01881/hitler_1881083c.jpg",
                "followed": false
            }]);
            expect(scope.users).toBeUndefined()
            expect($httpBackend.flush).toThrow("No pending request to flush !");
            expect(scope.users).toBeUndefined();
        });
    });

    //tests for user list controller
    describe('UserListCtrl', function () {
        var scope, ctrl, $httpBackend;

        var users = [
        {
            "userName": "ivanov",
            "name": "John Doe",
            "profilePicture": "http://images.primewire.ag/thumbs/12135_John_Doe_2002.jpg",
            "followed": false
        },
        {
            "userName": "ivanov",
            "name": "Jack Sparrow",
            "profilePicture": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTijTSkcXXSPI96CRdUBQb21hc9Xva3B25Gr8_g2RWGVz71e_JDFg",
            "followed": true
        },
        {
            "userName": "ivanov",
            "name": "Captain America",
            "profilePicture": "http://static.comicvine.com/uploads/original/8/80111/3259566-captain-america-large.jpg",
            "followed": true
        },
        {
            "userName": "ivanov",
            "name": "Che Guevara",
            "profilePicture": "http://old2.offnews.bg/files/uploads/2012/10/280px-CheHigh-280x360.jpg",
            "followed": true
        },
        {
            "userName": "ivanov",
            "name": "Joseph Stalin",
            "profilePicture": "http://www.historytoday.com/sites/default/files/stalin_0.jpg",
            "followed": false
        },
        {
            "userName": "ivanov",
            "name": "Adolf Hitler",
            "profilePicture": "http://i.telegraph.co.uk/multimedia/archive/01881/hitler_1881083c.jpg",
            "followed": true
        },
        {
            "userName": "ivanov",
            "name": "Ivan Draganov",
            "profilePicture": "http://www.omda.bg/uploaded_files/image/drago.jpg",
            "followed": true
        }];


        beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
            $httpBackend = _$httpBackend_;
            $httpBackend.expectGET('json/users.json').
                respond(users);

            scope = $rootScope.$new();
            sessionStorage.getItem = function () {
                return "12312";
            };
            ctrl = $controller('UserListCtrl', { $scope: scope });
        }));

        it('should get data from for all users', function () {
            expect(scope.users).toEqualData([]);
            $httpBackend.flush();

            expect(scope.users).toEqualData(users);
        });
    });

    //tests for user profile controller
    describe('UserProfileCtrl', function () {
        var scope, $httpBackend, ctrl;

        beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
            $httpBackend = _$httpBackend_;

            sessionStorage.getItem = function () {
                return "12312";
            };

            scope = $rootScope.$new();
            ctrl = $controller('UserProfileCtrl', { $scope: scope, $routeParams: { userName: "adich" } });
        }));

        //fetch not friend user
        it('should fetch not friend user', function () {
            $httpBackend.expectGET('json/users/adich.json').respond({
                "userName": "adich",
                "name": "Adolf Hitler",
                "profilePicture": "http://i.telegraph.co.uk/multimedia/archive/01881/hitler_1881083c.jpg",
                "followed": false
            });

            expect(scope.user).toEqualData({});
            
          


            //$httpBackend.flush();

            //expect(scope.user).toEqualData({
            //    "userName": "adich",
            //    "name": "Adolf Hitler",
            //    "profilePicture": "http://i.telegraph.co.uk/multimedia/archive/01881/hitler_1881083c.jpg",
            //    "followed": false
            //});
        });

        //fetch friend user
        it('should fetch friend user', function () {
            $httpBackend.expectGET('json/users/adich.json').respond({
                "userName": "adich",
                "name": "Adolf Hitler",
                "profilePicture": "http://i.telegraph.co.uk/multimedia/archive/01881/hitler_1881083c.jpg",
                "followed": true
            });
            $httpBackend.expectGET('json/books.json?userName=adich').respond(["book1", "book2", "book3"]);

            expect(scope.user).toEqualData({});
            //    $httpBackend.flush();

            //    expect(scope.user.books).toEqualData(["book1", "book2", "book3"]);
        });

        //fetch my user
        it('should fetch my user', function () {
            $httpBackend.expectGET('json/users/adich.json').respond({
                "userName": "adich",
                "firstName": "Adolf",
                "firstName": "Hitler",
                "profilePicture": "http://i.telegraph.co.uk/multimedia/archive/01881/hitler_1881083c.jpg",
            });
            sessionStorage.getItem = function () {
                return "12312";
            };


            scope.isMyProfile = function () { return true; };

            $httpBackend.expectGET('json/books.json?userName=adich').respond(["book1", "book2", "book3"]);

            expect(scope.user).toEqualData({});
            // $httpBackend.flush();

            ////expect(scope.user.books).toEqualData(["book1", "book2", "book3"]);
        });
    });
});