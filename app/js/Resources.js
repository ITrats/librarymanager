﻿var ModuleNames = {
    libraryManager: "libraryManager",
    libraryControllers: "libraryControllers",
    libraryDirectives: "libraryDirectives",
    libraryEffectControllers: "libraryEffectControllers",
    libraryAppServices: "libraryAppServices",
    libraryTranslator: "libraryTranslator",
    libraryFilters: "libraryFilters",
    ngRoute: "ngRoute",
    ngTouch: "ngTouch",
    ngAnimate: "ngAnimate",
    uiBootstrap: "ui.bootstrap",
    ngResource: "ngResource"
};

var CtrlNames = {
    AdvancedSearchCtrl: 'AdvancedSearchCtrl',
    BookDetailsCtrl: 'BookDetailsCtrl',
    BookListCtrl: "BookListCtrl",
    HomeCtrl: 'HomeCtrl',
    ProfileBookDetail: "ProfileBookDetail",
    RelatedProfilesCtrl: "RelatedProfilesCtrl",
    SearchCtrl: "SearchCtrl",
    TranslateController: "TranslateController",
    UserListCtrl: "UserListCtrl",
    UserProfileCtrl: "UserProfileCtrl",
    MyProfileBook: "MyProfileBook",
    GoogleAPICtrl: "GoogleAPICtrl",
    RelatedBookCtrl: "RelatedBookCtrl",
    RelatedProfileCtrl: "RelatedProfileCtrl",
    ImagePickerCtrl: "ImagePickerCtrl",
    RegistrationCtrl: "RegistrationCtrl",
    LoginCtrl: "LoginCtrl",
    LogoutCtrl: "LogoutCtrl",
    AddBookCtrl: "AddBookCtrl",
    AddReviewCtrl: "AddReviewCtrl",
    ForgotPasswordCtrl: "ForgotPasswordCtrl"
};

var ServiceNames = {
    $scope: "$scope",
    $rootScope: "$rootScope",
    BookService: "BookService",
    $location: "$location",
    $routeParams: "$routeParams",
    UserService: "UserService",
    DefaultLanguage: "DefaultLanguage",
    $translate: "$translate",
    $routeProvider: "$routeProvider",
    $resource: "$resource",
    $interval: "$interval",
    $filter: "$filter",
    GoogleAPI: "GoogleAPI",
    $http: "$http",
    $q: "$q",
    LoginService: "LoginService",
    LogoutService: "LogoutService",
    RegistrationService: "RegistrationService",
    NewBookService: "NewBookService",
    BookServiceTopTen: "BookServiceTopTen",
    NewReviewService: "NewReviewService",
    RelatedBookService: "RelatedBookService",
    $timeout: "$timeout",
    UpdateTimer: "UpdateTimer",
    UpdateBookStatus: "UpdateBookStatus",
    RelatedUserService: "RelatedUserService",
    ForgotPasswordService: "ForgotPasswordService"
};

var FilterNames = {
    advancedLocalSearch: "advancedLocalSearch",
    localSearch: "localSearch",
    sortBooksByStatus: "sortBooksByStatus"
};

var bookReadStatus = {
    isReadBook: "isReadBook",
    currentlyReading: "currentlyReading",
    toBeRead: "toBeRead"
};

var searchType = {
    advancedSearchLocal: "advancedSearchLocal",
    advancedSearh: "advancedSearh",
    searchInDB: "searchInDB",
    searchChanged: "searchChanged",
    updateUserRelation: "updateUserRelation"
};

var BookGroupTitleNames = {
    Reading: "Reading",
    FutureReadings: "Future readings",
    ReadBooks: "Read books"
};

var booksGenreList = ["Fantasy", "Horror", "Fiction", "Romance", "Adventure", "Biography", "Classics", "Comics", "History"];

