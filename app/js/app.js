"use strict";

/* App Module */
(function () {
    var app = angular.module(ModuleNames.libraryManager, [ModuleNames.ngRoute, ModuleNames.ngTouch, ModuleNames.ngAnimate, ModuleNames.uiBootstrap,
            ModuleNames.libraryDirectives, ModuleNames.libraryControllers, ModuleNames.libraryEffectControllers,
            ModuleNames.libraryAppServices, ModuleNames.libraryTranslator, ModuleNames.libraryFilters]);

    app.config([ServiceNames.$routeProvider, function ($routeProvider) {

        $routeProvider
            .when("/landing", {
                templateUrl: "partials/landing.html",
            })
            .when("/home", {
                templateUrl: "partials/home.html",
                controller: CtrlNames.HomeCtrl
            })
            .when("/books", {
                templateUrl: "partials/bookList.html",
                controller: CtrlNames.BookListCtrl
            })
            .when("/books/:bookId", {
                templateUrl: "partials/bookDetails.html",
                controller: CtrlNames.BookDetailsCtrl
            })
            .when("/users", {
                templateUrl: "partials/userList.html",
                controller: CtrlNames.UserListCtrl
            })
            .when("/users/:username", {
                templateUrl: "partials/userProfile.html",
                controller: CtrlNames.UserProfileCtrl
            })
            .otherwise({
                redirectTo: "/home"
            });
    }]);
})();