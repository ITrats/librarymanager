'use strict';

/* Controllers */

(function () {
    var app = angular.module(ModuleNames.libraryControllers, []);

    //// isHome is a gobal method that can determine if the user is on the home page
    app.run(function ($rootScope, $location) {

        $rootScope.isHome = function () {
            if ($location.url() === '/home') {
                return true;
            }
            else {
                return false;
            }
        }

        if (sessionStorage.getItem("token") == undefined) {
            $location.url("/landing");
        }

        $rootScope.userName = sessionStorage.getItem('username');

    });

    app.run(function ($rootScope, BookService) {
        //method for subscribing for the search.
        //by changing textbox text you change filter value of current list
        //by pressing search you fetch data from server by passing typed text as url parameter
        $rootScope.subscribeForSearch = function (scope, service, location) {
            scope.$on(searchType.searchChanged, function (event, searchDetails) {
                scope.localFilter = searchDetails.searchedText;
            });

            scope.$on(searchType.searchInDB, function (event, searchDetails) {
                scope.localFilter = "";

                if (scope.books !== undefined && searchDetails.searchBook) {
                    if (location.url() !== "/books") {
                        location.url("/books");
                        app.searchResult = service.query({
                            searchedBookAuthor: searchDetails.searchedText
                        });
                    }
                    else {
                        scope.books = service.query({
                            searchedBookAuthor: searchDetails.searchedText
                        });
                    }
                }
                if (scope.users !== undefined && searchDetails.searchUser) {
                    if (location.url() !== "/users") {
                        location.url("/books");
                        app.searchResult = BookService.query({
                            searchedUser: searchDetails.searchedText
                        });
                    }
                    else {
                        scope.users = service.query({
                            searchedUser: searchDetails.searchedText
                        });
                    }
                }
            });

            scope.$on(searchType.advancedSearchLocal, function (event, searchDetails) {
                scope.advancedLocalFilter = searchDetails;
            });

            ///redirect to books page ang show search result
            scope.$on(searchType.advancedSearh, function (event, searchDetails) {
                scope.advancedLocalFilter = null;
                if (location.url() !== "/books") {
                    location.url("/books");
                    app.searchResult = BookService.query({
                        advancedSearchDetails: searchDetails
                    });
                }
                else {
                    scope.books = BookService.query({
                        advancedSearchDetails: searchDetails
                    });
                }
            });
        }


        //todo - when have server to decide whether to use this
        $rootScope.subscribeForUserRelationChange = function (scope) {
            scope.$on(searchType.updateUserRelation, function (event, userInfo) {
                console.log(userInfo.userName);
              
            });
        };
    });
})();