﻿'use strict';
(function () {
    angular.module(ModuleNames.libraryFilters).filter(FilterNames.sortBooksByStatus, function () {
        return function (books, expectedStatus) {
            var filtered = [];

            angular.forEach(books, function (book) {
                if (book.status === expectedStatus) {
                    filtered.push(book);
                }
            });
            return filtered;
        };
    });
})();