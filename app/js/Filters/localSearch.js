﻿'use strict';
(function () {
    //custom local search filter: searches user from user list by Name 
    //searches book from booklist by author and book title
    //case insensitive
    angular.module(ModuleNames.libraryFilters).filter(FilterNames.localSearch, function () {
        return function (elements, filterText) {
            if (!filterText) {
                return elements;
            }

            var ft = filterText.toLowerCase();
            var filtered = [];

            angular.forEach(elements, function (element) {
                if (element.username){
                    if (element.firstName.toLowerCase().includes(ft)) {
                        filtered.push(element);
                    }
                    else if (element.lastName.toLowerCase().includes(ft)) {
                        filtered.push(element);
                    }
                }
                else if (element.title) {
                    if (element.title.toLowerCase().includes(ft)) {
                        filtered.push(element);
                    }
                    else if (element.author.toLowerCase().includes(ft)) {
                        filtered.push(element);
                    }
                }
            });
            return filtered;
        };
    });
})();