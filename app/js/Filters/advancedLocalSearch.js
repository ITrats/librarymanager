﻿'use strict';
(function () {
    angular.module(ModuleNames.libraryFilters).filter(FilterNames.advancedLocalSearch, function () {
        return function (books, filterDetails) {
            if (!filterDetails) {
                return books;
            }
            var filtered = [];

            angular.forEach(books, function (book) {
                var bookPassedFilters = true;

                function checkForTitle() {
                    if (!filterDetails.title) {
                        return true;
                    }

                    var bookTitle = book.title.toLowerCase();
                    var filterTitleField = filterDetails.title.toLowerCase();

                    if (bookTitle.includes(filterTitleField)) {
                        return true;
                    }
                    return false;
                }

                function checkForAuthor() {
                    if (!filterDetails.author) {
                        return true;
                    }

                    var bookAuthor = book.author.toLowerCase();
                    var filterTitleField = filterDetails.author.toLowerCase();

                    if (bookAuthor.includes(filterTitleField)) {
                        return true;
                    }
                    return false;
                }

                function checkForGenre() {
                    if (!filterDetails.genres || filterDetails.genres.length === 0) {
                        return true;
                    }

                    if (filterDetails.genres.indexOf(book.genre) !== -1) {
                        return true;
                    }
                    return false;
                }

                function checkForStatus() {
                    if (!filterDetails.status) {
                        return true;
                    }

                    var bookStatus = book.status.toLowerCase();
                    var filterStatusField = filterDetails.status.toLowerCase();

                    if (bookStatus.includes(filterStatusField)) {
                        return true;
                    }
                    return false;
                }

                function checkForReadDate() {
                    if (!filterDetails.status || filterDetails.status !== "isReadBook" || !filterDetails.readDate) {
                        return true;
                    }
                    var convertedDate = new Date(book.publishedDate);

                    if (convertedDate >= filterDetails.readDate) {
                        return true;
                    }
                    return false;
                }


                if (checkForTitle() && checkForAuthor() && checkForStatus() && checkForGenre() && checkForReadDate()) {
                    filtered.push(book);
                }
            });
            return filtered;
        };
    });
})();