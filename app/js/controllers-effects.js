'use strict';

/* Controllers */

(function () {
    var app = angular.module("libraryEffectControllers", []);

    //// Controller for the carousel (image slider)
    app.controller("CarouselCtrl", ["$scope",
      function ($scope) {

          $scope.myInterval = 5000;

          var slides = [];
          for (var i = 0; i < $scope.$parent.$parent.books.length; i++) {
              slides[i] = {
                  image: $scope.$parent.$parent.books[i].cover,
                  title: $scope.$parent.$parent.books[i].title,
                  author: $scope.$parent.$parent.books[i].author,
                  id: $scope.$parent.$parent.books[i].id
              };
          }

          $scope.slides = slides;
      }]);

    //// Controller for opening modal window
    app.controller("ModalCtrl", function ($scope, $modal) {

        var modalInstance = null;
        var sideOpened = false;

        $scope.animationsEnabled = true;

        $scope.open = function (size, type) {

            modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: "directives/" + type + ".html",
                controller: "ModalInstanceCtrl",
                size: size
            });
        }

        $scope.openSidebar = function () {
            if (modalInstance !== null) {
                if (modalInstance.result.$$state.status == 2) {
                    sideOpened = false;
                };
            }
            else {
                sideOpened = false;
            }

            if (sideOpened == false) {

                modalInstance = $modal.open({
                    animation: $scope.animationsEnabled,
                    backdropClass: "modal-side",
                    windowClass: "sidebar-transition",
                    templateUrl: "directives/sidebar.html",
                    controller: "ModalInstanceCtrl",
                    size: 'side'
                });

                sideOpened = true;
            }



        }

        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        }

    });

    // Please note that $modalInstance represents a modal window (instance) dependency.
    // It is not the same as the $modal service used above.

    app.controller('ModalInstanceCtrl', function ($scope, $modalInstance) {

        $scope.ok = function () {
            $modalInstance.close($scope.selected.item);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    });


    //// Controller for tabs
    app.controller('TabsCtrl', function ($scope, $window) {
        // Controller is needed for rendering of tabs albeit being empty
    });


    //// Controller for Menu effect
    app.controller('MenuCtrl', function ($scope, $window) {
        $scope.isCollapsed = true;
        $scope.isSearchCollapsed = true;
        $scope.scroll = 0;
    });


    //// Dropdown menu controller
    app.controller('DropdownCtrl', function ($scope) {

        $scope.status = {
            isopen: false
        };


        $scope.toggleDropdown = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.status.isopen = !$scope.status.isopen;
        };
    });


    app.controller("GenreCtrl", function ($rootScope, $scope) {
        $scope.bookGenres = booksGenreList;
        $scope.boxColors = ["F4794F", "2e0927", "04756f", "F46A6A", "A9A4F4", "E5B64A", "48a560", "4AC6E5", "70A4F4"];

        $scope.fetchFromServer = function (clickedGenre) {
            $rootScope.$broadcast(searchType.advancedSearh,
                {
                    genres: [clickedGenre]
                });
        };
    });



    app.controller("RatingCtrl", function ($scope) {
        
        $scope.$parent.review.rate = 7;
        $scope.max = 10;

        $scope.hoveringOver = function(value) {
        $scope.overStar = value;
        $scope.percent = 100 * (value / $scope.max);
        };

        $scope.ratingStates = [
            {stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'},
            {stateOff: 'glyphicon-off'}
        ];
    });

})();