"use strict";

/* Directives */

(function () {
    var app = angular.module(ModuleNames.libraryDirectives, []);

    app.directive("mainMenu", function () {
        return {
            restrict: "E",
            templateUrl: "directives/menu.html"
        };
    });

    app.directive("search", function () {
        return {
            restrict: "E",
            templateUrl: "directives/search.html"
        };
    });

    app.directive("advancedSearch", function () {
        return {
            restrict: "E",
            templateUrl: "directives/advancedSearch.html"
        };
    });

    app.directive("topPicks", function () {
        return {
            restrict: "E",
            templateUrl: "directives/top-picks.html"
        };
    });

    app.directive("userInfoTabControl", function () {
        return {
            restruct: "E",
            templateUrl: "directives/user/userInfoTabControl.html"
        };
    });

    app.directive("myBookList", function () {
        return {
            restrict: "E",
            templateUrl: "directives/book/myBookList.html"
        };
    });

    app.directive("followedUserBookList", function () {
        return {
            restrict: "E",
            templateUrl: "directives/book/followedUserBookList.html"
        };
    });

    app.directive("relatedProfiles", function () {
        return {
            restrict: "E",
            templateUrl: "directives/user/relatedProfiles.html"
        };
    });

    app.directive("bookDetail", function () {
        return {
            restrict: "E",
            templateUrl: "directives/book/bookDetail.html"
        };
    });

    app.directive("imagePicker", function () {
        return {
            restrict: "E",
            templateUrl: "directives/imagePicker.html"
        };
    });

    //// Scroll position directive manipulates catches current stroll position
    app.directive("scrollPosition", function($window) {
        return {
            scope: {
                scroll: "=scrollPosition"
            },
            link: function(scope, element, attrs) {
                var windowEl = angular.element($window);
                var handler = function() {
                    scope.scroll = document.body.scrollTop;
                }
                windowEl.on("scroll", scope.$apply.bind(scope, handler));
                handler();
            }
        };
    });

})();