﻿'use strict';

(function () {
    ///Controller for my profile books categories horizontal scroll views
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.ProfileBookDetail, [ServiceNames.$scope, ServiceNames.$rootScope,
       function ($scope, $rootScope) {

           $scope.bookGroups = [{
               groupTitle: BookGroupTitleNames.Reading,
               bookStatus: bookReadStatus.currentlyReading
           }, {
               groupTitle: BookGroupTitleNames.FutureReadings,
               bookStatus: bookReadStatus.toBeRead
           }, {
               groupTitle: BookGroupTitleNames.ReadBooks,
               bookStatus: bookReadStatus.isReadBook
           }, ];
       }]);
})();
