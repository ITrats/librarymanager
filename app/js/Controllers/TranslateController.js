﻿'use strict';

(function () {
    //// Controller for changing site's display language
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.TranslateController, function ($translate, $scope, DefaultLanguage) {
        $scope.flagUrl = "img/flag-" + $translate.use() + ".jpg";
        $scope.changeLanguage = function (langKey) {
            $translate.use(langKey);
            $scope.flagUrl = "img/flag-" + langKey + ".jpg";
            DefaultLanguage.setLanguage(langKey);
        };
    });
})();