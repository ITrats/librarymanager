﻿/// <reference path="../../bower_components/angular/angular.js" />

(function () {
    //controller for the behavior of my profile book groups and items
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.RelatedBookCtrl,
    [ServiceNames.$scope, ServiceNames.$location, ServiceNames.RelatedBookService, ServiceNames.$rootScope, ServiceNames.$routeParams, ServiceNames.$filter,
function ($scope, $location, RelatedBookService, $rootScope, $routeParams, $filter) {
    $scope.getBtnName = function () {
        if ($scope.book.isInMyLibrary) {
            $scope.btnType = "remove";
            return $filter("translate")("REMOVEFROMLIBRARY");
        }
        else {
            $scope.btnType = "add";
            return $filter("translate")("ADDTOLIBRARY");;
        }
    };
    $scope.editMyLibrary = function () {
        $scope.book.isInMyLibrary = !$scope.book.isInMyLibrary;

        RelatedBookService.toggleBookLinkStatus({}, { bookId: $scope.book.id }, function (data) {
            console.log("SUCCESS!");
        },
        function (data) {
            console.log("FAIL!");
        });
    };
}]);
})();