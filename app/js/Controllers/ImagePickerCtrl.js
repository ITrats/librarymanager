﻿'use strict';

(function () {
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.ImagePickerCtrl,
     [ServiceNames.$scope, ServiceNames.$rootScope, ServiceNames.$location,
     function ($scope, $rootScope, $location) {

         var preview = document.getElementById('imagePreview');

         var bookUrl = "https://books.google.bg/googlebooks/images/no_cover_thumb.gif";
         var profileUrl = "http://www.phantomshockey.com/wp-content/uploads/2014/07/profile-icon.png";

         if ($scope.$parent.book == true) {
             preview.src = bookUrl;
         }
         else {
             preview.src = profileUrl;
         }

         $scope.isLanding = function () {
             if ($location.url() == "/landing") {
                 return true;
             }
             else {
                 return false;
             }
         }

         $rootScope.imageUrl = preview.src;

         document.getElementById("imagePicker").onchange = function () {
             var file = document.getElementById("imagePicker").files[0];
             var reader = new FileReader();

             reader.onloadend = function () {
                 preview.src = reader.result;

                 $rootScope.imageUrl = reader.result;
             }

             if (file) {
                 reader.readAsDataURL(file);
             } else {

                 if ($scope.$parent.book == true) {
                     preview.src = bookUrl;
                 }
                 else {
                     preview.src = profileUrl;
                 }

                 $rootScope.imageUrl = preview.src;
             }
         }
     }]);
})();