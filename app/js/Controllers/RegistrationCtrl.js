﻿'use strict';

(function () {
    //// Controller that fetches a list of users
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.RegistrationCtrl,
        [ServiceNames.$rootScope, ServiceNames.$scope, ServiceNames.$location, ServiceNames.UserService, ServiceNames.$http, ServiceNames.RegistrationService, ServiceNames.$timeout,
        function ($rootScope, $scope, $location, UserService, $http, RegistrationService, $timeout) {

            document.getElementById("registrationForm").addEventListener("submit", function (event) {
                event.preventDefault();
            });

            $scope.validPassword = false;

            $scope.validatePassword = function () {
                $scope.validPassword = $scope.pass1 === $scope.pass2;
            }

            $scope.sendRegistrationForm = function () {
                $scope.showMsg = null;
                $scope.error = undefined;

                $scope.registerData = {
                    "avatar": $rootScope.imageUrl,
                    "username": $scope.userName,
                    "password": btoa($scope.pass1),
                    "passwordConfirmed": btoa($scope.pass2),
                    "firstName": $scope.firstName,
                    "lastName": $scope.lastName,
                    "email": $scope.email
                };



                function successfullRegister(response) {
                    $scope.$parent.cancel();
                }

                function failedRegister(response) {
                    $scope.showMsg = "-show";
                    if (response.data) {
                        $scope.error = response.data.message;
                    }
                    else {
                        $scope.error = "Something went wrong!";
                    }

                    $timeout(function () {
                        $scope.showMsg = null;
                        $scope.error = undefined;
                    }, 10000);
                }

                RegistrationService.query({}, $scope.registerData, successfullRegister, failedRegister);
            };
        }]);
})();