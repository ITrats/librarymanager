﻿"use strict";

(function () {
    //// Controller that fetches a list of users
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.LoginCtrl,
        [ServiceNames.$rootScope, ServiceNames.$scope, ServiceNames.$location, ServiceNames.UserService, ServiceNames.$http, ServiceNames.LoginService, ServiceNames.$timeout,
        function ($rootScope, $scope, $location, UserService, $http, LoginService, $timeout) {

            $rootScope.forgotPassword = false;

            $scope.forgot = function () {
                $rootScope.forgotPassword = true;
            }

            $scope.sendLoginForm = function () {
                $scope.showMsg = null;
                $scope.error = undefined;
                $scope.loginData = {
                    "username": $scope.userName,
                    "password": btoa($scope.password),
                };

                function successfullLogin(response) {
                    sessionStorage.setItem("token", response.token);
                    sessionStorage.setItem("userId", response.id);
                    sessionStorage.setItem("username", response.username);
                    $rootScope.myUserName = response.username;
                    $location.url("/home");
                    $scope.$parent.cancel();
                }

                function failedLogin(response) {
                    $scope.showMsg = "-show";
                    if (response.data) {
                        $scope.error = response.data.message;
                    }
                    else {
                        $scope.error = "Something went wrong!";
                    }

                    $timeout(function () {
                        $scope.showMsg = null;
                        $scope.error = undefined;
                    }, 10000);
                }
                LoginService.query({}, $scope.loginData, successfullLogin, failedLogin);
            };
        }]);
})();