﻿'use strict';

(function () {
    //// Controller that fetches a list of users
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.UserListCtrl,
        [ServiceNames.$rootScope, ServiceNames.$scope, ServiceNames.$location, ServiceNames.UserService,
        function ($rootScope, $scope, $location, UserService) {
            $scope.users = UserService.query();

            $rootScope.subscribeForSearch($scope, UserService, $location);
        }]);
})();