﻿'use strict';

(function () {
    //// Controller that fetches book's details
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.BookDetailsCtrl,
        [ServiceNames.$rootScope, ServiceNames.$scope, ServiceNames.$location, ServiceNames.BookService, ServiceNames.$routeParams,
    function ($rootScope, $scope, $location, BookService, $routeParams) {
        $scope.book = BookService.get({ bookId: $routeParams.bookId });
        console.log($scope.book);
        $rootScope.subscribeForSearch($scope, BookService, $location);
    }]);
})();