"use strict";

(function () {
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.AddReviewCtrl,
		[ServiceNames.$scope, ServiceNames.$rootScope, ServiceNames.$filter, ServiceNames.NewReviewService, ServiceNames.$routeParams,
    function ($scope, $rootScope, $filter, NewReviewService, $routeParams) {

        $scope.review = {};

        $scope.addReview = function () {
            console.log({ bookId: $routeParams.bookId, comment: $scope.review.text, rating: $scope.review.rate });
            NewReviewService.sendReview({}, { bookId: $routeParams.bookId, comment: $scope.review.text, rating: $scope.review.rate }, function () {
                $scope.$parent.cancel();
                console.log("success ");
                console.log($scope.review);
            }, function () {
                console.log("fail ");
                console.log($scope.review);
            });
        }
    }]);
})();