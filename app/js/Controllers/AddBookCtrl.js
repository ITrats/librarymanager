"use strict";

(function () {
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.AddBookCtrl,
		[ServiceNames.$scope, ServiceNames.$rootScope, ServiceNames.$filter, ServiceNames.NewBookService,
    function ($scope, $rootScope, $filter, NewBookService) {

        $rootScope.authorPlaceholder = $filter("translate")("AUTHOR");
        $rootScope.titlePlaceholder = $filter("translate")("TITLE");
        $rootScope.genrePlaceholder = $filter("translate")("GENRE");
        $rootScope.publishedPlaceholder = $filter("translate")("PUBLISHED");
        $rootScope.pageCountPlaceholder = $filter("translate")("PAGECOUNT");
        $rootScope.desciptionPlaceholder = $filter("translate")("DESCRIPTION");


        $rootScope.currentYear = new Date().getFullYear();

        $scope.newBook = {};
        $scope.newBook.author = {};

        $scope.addBook = function (fromGoogle, googleBook) {
            if (!fromGoogle) {
                $scope.newBook.cover = $rootScope.imageUrl;
            }
            else {
                $scope.newBook = googleBook;
            }

            NewBookService.sendBook([], $scope.newBook, function () {
                console.log("success ");
                //console.log($scope.newBook);

                $scope.$parent.cancel();
            }, function () {
                console.log("fail ");
                console.log($scope.newBook);
            });

        }
    }]);
})();