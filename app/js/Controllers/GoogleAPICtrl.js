"use strict";

(function () {
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.GoogleAPICtrl,
		[ServiceNames.$scope, ServiceNames.$rootScope, ServiceNames.GoogleAPI, ServiceNames.$filter,
    function ($scope, $rootScope, GoogleAPI, $filter) {


        $scope.displayResults = false;

        $scope.startIndex = 0;
        $scope.maxResults = 5;

        $scope.totalItems = null;

        $scope.error = false;
        $scope.fetching = false;

        $scope.googleSearch = function (startIndex) {
            $scope.error = false;
            $scope.displayResults = false;
            $scope.fetching = false;

            var query = "";
            if (startIndex != $scope.startIndex) {
                $scope.startIndex = startIndex;
            }

            if ($scope.googleTitle != undefined && $scope.googleTitle != "") {
                query += "intitle:" + $scope.googleTitle;
            }
            if ($scope.googleAuthor != undefined && $scope.googleAuthor != "") {
                query += "+inauthor:" + $scope.googleAuthor;
            }
            var end = false;
            var counter = 0;
            if (query != "") {

                var requestsCount = 0;

                (function resolveRequest() {
                    $scope.googleBooks = GoogleAPI.get({ q: query, maxResults: $scope.maxResults, startIndex: startIndex }).$promise
                    .then(
                    function (data) {
                        console.log("startIndex = " + startIndex + " maxResults = " + $scope.maxResults + " totalItems = " + data.totalItems);
                        $scope.totalItems = data.totalItems;
                        //console.log($scope.lastIndex);

                        if (data.items != undefined) {
                            $scope.googleBooks = convertBooks(data);
                            $scope.displayResults = true;
                            $scope.error = false;
                            $scope.lastIndex = startIndex;
                            $scope.fetching = false;
                        }
                        else {

                            $scope.displayResults = false;
                            requestsCount++;

                            if (requestsCount < 21) {
                                $scope.fetching = true;
                                resolveRequest();
                            }
                            
                            else {
                                $scope.error = true;
                                $scope.fetching = false;
                            }
                        }
                    });
                })();
            }
        }

        function convertBooks(googleBooks) {

            var books = [];

            //console.log(googleBooks);

            for (var i in googleBooks.items) {


                books[i] = {};

                books[i].title = googleBooks.items[i].volumeInfo.title;
                
                if (googleBooks.items[i].volumeInfo.authors) {
                    books[i].author = {
                        name: googleBooks.items[i].volumeInfo.authors[0]
                    };
                }
                
                books[i].numberOfPages = googleBooks.items[i].volumeInfo.pageCount || null;

                if (googleBooks.items[i].volumeInfo.description != undefined) {
                    books[i].description = googleBooks.items[i].volumeInfo.description;
                }

                if (googleBooks.items[i].volumeInfo.publishedDate != undefined) {
                    books[i].yearPublished = googleBooks.items[i].volumeInfo.publishedDate.slice(0, 4);
                }
                else {
                    books[i].yearPublished = null;
                }

                if (googleBooks.items[i].volumeInfo.imageLinks != undefined) {
                    books[i].cover = googleBooks.items[i].volumeInfo.imageLinks.thumbnail;
                }
                else {
                    books[i].cover = "https://books.google.bg/googlebooks/images/no_cover_thumb.gif";
                }
            }
            return books;
        }


    }]);
})();