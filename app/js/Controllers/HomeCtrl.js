﻿/// <reference path="../Resources.js" />
'use strict';

(function () {
    ///controller for home page
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.HomeCtrl,
        [ServiceNames.$rootScope, ServiceNames.$scope, ServiceNames.$location, ServiceNames.BookService,
      function ($rootScope, $scope, $location, BookService) {
          if ($rootScope.isHome()) {
              if (!sessionStorage.getItem("token")) {
                  $location.url("/landing");
              }
              else {
                  $scope.books = BookService.query({ "top-picks": 10 });
                  $rootScope.subscribeForSearch($scope, BookService, $location);
              }
          }
      }]);
})();