"use strict";

(function () {
    //// Controller that fetches a list of users
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.ForgotPasswordCtrl,
        [ServiceNames.$rootScope, ServiceNames.$scope, ServiceNames.$location, ServiceNames.ForgotPasswordService, ServiceNames.$http, ServiceNames.$timeout,
        function ($rootScope, $scope, $location, ForgotPasswordService, $http, $timeout) {

            $rootScope.sendForgotForm = function () {
                
                $scope.showMsg = false;
                $scope.error = undefined;
                $scope.success = false;

                $scope.forgotData = {
                    "username": $scope.userName,
                    "email": $scope.email
                };

                function success(response) {
                    $scope.showMsg = true;
                    $scope.success = true;

                    $scope.msg = "Success, please check your email!";

                    $timeout(function () {
                        $scope.showMsg = false;
                        $scope.msg = undefined;
                        $rootScope.forgotPassword = false;
                        $scope.success = false;
                    }, 5000);
                    
                }

                function fail(response) {

                    $scope.showMsg = true;
                    if (response.data) {
                        $scope.msg = response.data.message;
                    }
                    else {
                        $scope.msg = "Something went wrong!";
                    }

                    $timeout(function () {
                        $scope.showMsg = false;
                        $scope.msg = undefined;
                    }, 10000);
                }
                ForgotPasswordService.query({}, $scope.forgotData, success, fail);
            };
        }]);
})();