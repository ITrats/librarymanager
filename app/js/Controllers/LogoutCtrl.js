﻿'use strict';

(function () {
    //// Controller that fetches a list of users
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.LogoutCtrl,
        [ServiceNames.$rootScope, ServiceNames.$scope, ServiceNames.$location, ServiceNames.UserService, ServiceNames.$http, ServiceNames.LogoutService,
        function ($rootScope, $scope, $location, UserService, $http, LogoutService) {

            $scope.logout = function () {
                function successfullLogout(response) {
                    sessionStorage.removeItem("token");
                    sessionStorage.removeItem("userId");
                    sessionStorage.removeItem("username");
                    $scope.$parent.cancel();
                    $location.url("/landing");
                }

                function failedLogout(response) {
                }
                LogoutService.query({}, "", successfullLogout, failedLogout);
            };
        }]);
})();