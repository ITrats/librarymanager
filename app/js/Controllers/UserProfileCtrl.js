﻿/// <reference path="../Resources.js" />
'use strict';

(function () {
    //// Controller that fetches user's details
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.UserProfileCtrl,
        [ServiceNames.$rootScope, ServiceNames.$scope, ServiceNames.$location, ServiceNames.UserService, ServiceNames.BookService, ServiceNames.$routeParams,
        function ($rootScope, $scope, $location, UserService, BookService, $routeParams) {

            $scope.user = UserService.get({ username: $routeParams.username });

            $scope.user.$promise.then(function () {
                if ($scope.isMyProfile() || $scope.user.followed === true) {
                    $scope.user.books = BookService.query();
                }
            });

            $scope.isMyProfile = function () {
                if (sessionStorage.getItem('username') === $scope.user.username) {
                    return true;
                }
                return false;
            };

            $rootScope.subscribeForSearch($scope, undefined, $location);
        }]);
})();