﻿/// <reference path="../../bower_components/angular/angular.js" />

(function () {
    //controller for the behavior of my profile book groups and items
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.MyProfileBook,
    [ServiceNames.$scope, ServiceNames.$location, ServiceNames.BookService, ServiceNames.$interval, ServiceNames.UpdateTimer, ServiceNames.UpdateBookStatus,
function ($scope, $location, BookService, $interval, UpdateTimer, UpdateBookStatus) {
    $scope.timerStarted = false;

    $scope.openBookDetails = function () {
        $location.url("books/" + $scope.book.id);
    };

    $scope.getTimerBtnName = function () {
        if ($scope.timerStarted) {
            return "Stop timer";
        }
        return "Start Timer";
    };

    $scope.getStartReadBtnName = function () {
        if ($scope.book.status) {
            if ($scope.book.status === bookReadStatus.toBeRead) {
                return "Start Reading!";
            }
            else if ($scope.book.status === bookReadStatus.isReadBook) {
                return "Read Again?";
            }
        }
    };

    $scope.startReading = function () {
        if ($scope.book.status && $scope.book.status !== bookReadStatus.currentlyReading) {
            $scope.book.status = bookReadStatus.currentlyReading;
            updateStatus();
        }
    };

    $scope.isCurrentlyReaded = function () {

        if ($scope.book.status && $scope.book.status !== bookReadStatus.currentlyReading) {
            return false;
        }
        return true;
    }

    function updateStatus() {
        UpdateBookStatus.update({}, { bookId: $scope.book.id, bookStatus: $scope.book.status }, function (data) {
        },
            function (data) {
            });
    }

    $scope.statusChange = function () {
        //get from select the option and set to it
        $scope.book.status = $scope.changedBookStatus;
        updateStatus();
        //todo to update server
    };

    //To Do to update from db etc..
    var stop;
    $scope.startTimer = function () {
        $scope.timerStarted = !$scope.timerStarted;

        UpdateTimer.startStopTimer({}, { bookId: $scope.book.id }, function (data) {
            $scope.elapsedTimeFromDb = data.savedTime;
        },
            function (data) {
            });

        if ($scope.timerStarted) {
            $scope.starTime = new Date();
            stop = $interval(updateTimer, 1000)
        }
        else {
            $interval.cancel(stop);
        }

        function updateTimer() {
            $scope.currentTime = new Date();
            var elapsed = new Date($scope.currentTime - $scope.starTime + $scope.elapsedTimeFromDb);

            $scope.time = elapsed.getHours() - 2 + ":" + elapsed.getMinutes() + ":" + elapsed.getSeconds();
        }

    };
}]);
})();