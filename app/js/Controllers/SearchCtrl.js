﻿'use strict';

(function () {
    //// Controller for searching
    ///when textbox text changes it broadcasts for this, so subscribers can update their fields
    ///when search is clicked broadcasts for this, so 
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.SearchCtrl, [ServiceNames.$scope, ServiceNames.$rootScope, ServiceNames.$filter,
        function ($scope, $rootScope, $filter) {

            $scope.searchPlaceholder = $filter("translate")("SEARCH");

            //broadcasts with specified header. subscribers can identify broadcasts given headers
            $scope.broadCastTo = function (broadCastHeader) {
                $rootScope.$broadcast(broadCastHeader,
                    { searchedText: $scope.searchedText, searchUser: $scope.searchUser, searchBook: $scope.searchBook });
            }
        }]);
})();