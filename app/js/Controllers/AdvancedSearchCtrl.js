﻿'use strict';

(function () {
    //// Controller for advanced searching
    /// filter/search by author, title, book read status, genres, 
    /// can filter by book read date if the book is read 
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.AdvancedSearchCtrl, [ServiceNames.$scope, ServiceNames.$rootScope, ServiceNames.BookService,
    function ($scope, $rootScope, BookService) {
        $scope.genreList = booksGenreList;

        $scope.checkedGenres = [];

        //if read selected to show read date
        $scope.readSelected = function () {
            if ($scope.selectedStatus && bookReadStatus.isReadBook && $scope.selectedStatus === bookReadStatus.isReadBook) {
                return true;
            }
            return false;
        };

        $scope.checkBoxSelectionChanged = function (genre) {
            if ($scope.checkedGenres.indexOf(genre) === -1) {
                $scope.checkedGenres.push(genre);
            }
            else {
                $scope.checkedGenres.splice($scope.checkedGenres.indexOf(genre), 1);
            }
        };

        $scope.advancedSearchLocal = function () {
            var readDate = $scope.bookReadDate ? $scope.bookReadDate.getTime() : undefined;
            $rootScope.$broadcast(searchType.advancedSearchLocal,
                {
                    title: $scope.title,
                    author: $scope.author,
                    genres: $scope.checkedGenres,
                    status: $scope.selectedStatus,
                    readDate: readDate
                });
        };

        //watch for changes in advanced search and updade locally
        $scope.$watch(function () {
            $scope.advancedSearchLocal();
        });

        $scope.fetchFromServer = function () {
            //broadcasts with specified header. subscribers can identify broadcasts given headers
            var readDate = $scope.bookReadDate ? $scope.bookReadDate.getTime() : undefined;
            $rootScope.$broadcast(searchType.advancedSearh,
                {
                    title: $scope.title,
                    author: $scope.author,
                    genres: $scope.checkedGenres,
                    status: $scope.selectedStatus,
                    readDate: readDate
                });
        };
    }]);
})();