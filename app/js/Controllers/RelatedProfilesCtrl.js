﻿'use strict';

(function () {
    //// Fetches only related users (followed/followint)
    //TODO - get all at one time
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.RelatedProfilesCtrl,
        [ServiceNames.$rootScope, ServiceNames.$scope, ServiceNames.$location, ServiceNames.UserService,
        function ($rootScope, $scope, $location, UserService) {
            if ($scope.user.followed === true || $scope.user.followed == undefined) {
                $scope.users = UserService.query({
                    userList: "related"
                });
                $rootScope.subscribeForSearch($scope, UserService, $location);
            }
        }]);
})();