﻿'use strict';

(function () {
    //// Controller that fetches a list of books
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.BookListCtrl,
        [ServiceNames.$rootScope, ServiceNames.$scope, ServiceNames.$location, ServiceNames.BookService,
    function ($rootScope, $scope, $location, BookService) {
        /// searchResult is needed for the case when user is not at books page and fetches data from db
        /// searchResult gets initialised when user clicks search from db from page, different from /books
        $scope.books = angular.module(ModuleNames.libraryControllers).searchResult;

        $rootScope.subscribeForSearch($scope, BookService, $location);
    }]);
})();