﻿'use strict';

(function () {
    angular.module(ModuleNames.libraryControllers).controller(CtrlNames.RelatedProfileCtrl,
        [ServiceNames.$rootScope, ServiceNames.$scope, ServiceNames.$location, ServiceNames.UserService, ServiceNames.RelatedUserService,
    function ($rootScope, $scope, $location, UserService, RelatedUserService) {
        $rootScope.subscribeForUserRelationChange($scope);

        $scope.updateUserRelation = function () {
            $scope.user.followed = !$scope.user.followed;
            //todo: decide whether to put service here or in app.run method

            RelatedUserService.toggleUserLinkStatus({}, { username: $scope.user.username }, function (data) {
                console.log("SUCCESS!");
            },
            function (data) {
                console.log("FAIL!");
            });

            $rootScope.$broadcast(searchType.updateUserRelation,
               { userName: $scope.user.userName, followed: $scope.user.followed });
        };
    }
        ]);
})();