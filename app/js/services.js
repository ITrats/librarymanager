/// <reference path="../bower_components/angular-resource/angular-resource.js" />
/// <reference path="../bower_components/angular/angular.js" />
/// <reference path="Resources.js" />

'use strict';

/* Services */
(function () {
    var jsonStubUrl = "http://jsonstub.com/"; //ServerURL variable for easy switching between jsonstub and the real server
    var serverUrl = "json/";
    var json = ".json";

    var libraryAppServices = angular.module(ModuleNames.libraryAppServices, [ModuleNames.ngResource]);

    //login service - post method send data user name and pass
    libraryAppServices.factory(ServiceNames.LogoutService, [ServiceNames.$resource,
      function ($resource) {
          return $resource(jsonStubUrl + "user/logout", {}, {
              query: {
                  method: "POST",
                  params: {},
                  headers: {
                      "Content-Type": "application/json",
                      'JsonStub-User-Key': 'c439a796-888c-4ffc-9753-e31381ca18d4',
                      'JsonStub-Project-Key': '8f7dfc47-cb2a-4e93-b8ff-51c9101b009a',
                      token: sessionStorage.getItem('token'),
                      userId: sessionStorage.getItem('userId')
                  }
              },
              data: "",
              dataType: "json"
          });
      }]);


    //login service - post method send data user name and pass
    libraryAppServices.factory(ServiceNames.LoginService, [ServiceNames.$resource,
      function ($resource) {
          return $resource(jsonStubUrl + "user/login", {}, {
              query: {
                  method: "POST",
                  params: {},
                  headers: {
                      "Content-Type": "application/json",
                      'JsonStub-User-Key': 'c439a796-888c-4ffc-9753-e31381ca18d4',
                      'JsonStub-Project-Key': '8f7dfc47-cb2a-4e93-b8ff-51c9101b009a',
                      token: sessionStorage.getItem('token'),
                      userId: sessionStorage.getItem('userId')
                  }
              },
              data: "",
              dataType: "json"
          });

      }]);

    //register service - post method send data user name, pass, pass confirm, first last name, avatar and email
    libraryAppServices.factory(ServiceNames.RegistrationService, [ServiceNames.$resource, function ($resource) {
        return $resource(jsonStubUrl + "user/register", {}, {
            query: {
                method: "POST",
                params: {},
                headers: {
                    'Content-Type': 'application/json',
                    'JsonStub-User-Key': 'c439a796-888c-4ffc-9753-e31381ca18d4',
                    'JsonStub-Project-Key': '8f7dfc47-cb2a-4e93-b8ff-51c9101b009a',
                    token: sessionStorage.getItem('token'),
                    userId: sessionStorage.getItem('userId')
                },
            },
            data: "",
            dataType: "json"
        });
    }]);



  //Forgotten password service - post method send data user name and pass
    libraryAppServices.factory(ServiceNames.ForgotPasswordService, [ServiceNames.$resource,
      function ($resource) {
          return $resource(jsonStubUrl + "user/forgot", {}, {
              query: {
                  method: "POST",
                  params: {},
                  headers: {
                      "Content-Type": "application/json",
                      'JsonStub-User-Key': '6bc8e291-2552-43ce-b098-379c6cee8fb6',
                      'JsonStub-Project-Key': '2db7f054-a60c-4fe3-ae45-297c6342fe71',
                      token: sessionStorage.getItem('token'),
                      userId: sessionStorage.getItem('userId')
                  }
              },
              data: "",
              dataType: "json"
          });

      }]);



    //service for retrieving book details and book lists
    libraryAppServices.factory(ServiceNames.BookService, [ServiceNames.$resource,
      function ($resource) {
          return $resource(serverUrl + "books/:bookId" + json, {}, {
              query: {
                  method: "GET",
                  params: {},
                  isArray: true,
                  headers: {
                      "Content-Type": "application/json",
                      token: sessionStorage.getItem('token'),
                      userId: sessionStorage.getItem('userId')
                  }
              },
              data: "",
              dataType: "json"
          });
      }]);

    //service for retrieving user details and user lists
    libraryAppServices.factory(ServiceNames.UserService, [ServiceNames.$resource,
     function ($resource) {
         return $resource(serverUrl + "users/:username" + json, {}, {
             query: {
                 method: "GET",
                 params: {},
                 isArray: true,
                 headers: {
                     "Content-Type": "application/json",
                     token: sessionStorage.getItem("token"),
                     userId: sessionStorage.getItem("userId")
                 },
                 data: "",
                 dataType: "json",
             }
         });
     }]);



    //possible variations - when connected to a server to use userservice with put method
    //service for lining existing user as followed user
    libraryAppServices.factory(ServiceNames.RelatedUserService, [ServiceNames.$resource,
      function ($resource) {
          return $resource(jsonStubUrl + "toggleUserLinkStatus/:username", {}, {
              toggleUserLinkStatus: {
                  method: "PUT",
                  params: { username: "" },
                  isArray: false,
                  headers: {
                      "Content-Type": "application/json",
                      'JsonStub-User-Key': 'c439a796-888c-4ffc-9753-e31381ca18d4',
                      'JsonStub-Project-Key': '8f7dfc47-cb2a-4e93-b8ff-51c9101b009a',
                      token: sessionStorage.getItem('token'),
                      userId: sessionStorage.getItem("userId")
                  }
              },
              data: "",
              dataType: "json"
          });
      }]);





    //possible variations - when connected to a server to use bookservice with put method
    //service for lining existing book to my library
    libraryAppServices.factory(ServiceNames.RelatedBookService, [ServiceNames.$resource,
      function ($resource) {
          return $resource(jsonStubUrl + "toggleBookLinkStatus/:bookId", {}, {
              toggleBookLinkStatus: {
                  method: "PUT",
                  params: { bookId: "" },
                  isArray: false,
                  headers: {
                      "Content-Type": "application/json",
                      'JsonStub-User-Key': 'c439a796-888c-4ffc-9753-e31381ca18d4',
                      'JsonStub-Project-Key': '8f7dfc47-cb2a-4e93-b8ff-51c9101b009a',
                      token: sessionStorage.getItem('token'),
                      userId: sessionStorage.getItem("userId")
                  }
              },
              data: "",
              dataType: "json"
          });
      }]);


    //// Default language provider that saves user's prefrences in localStorage
    libraryAppServices.provider(ServiceNames.DefaultLanguage, function () {
        var language = "";

        Storage.prototype.setObj = function (key, obj) {
            return this.setItem(key, JSON.stringify(obj));
        }
        Storage.prototype.getObj = function (key) {
            return JSON.parse(this.getItem(key));
        }

        return {
            loadLanguage: function () {
                if (localStorage["language"] != null) {
                    language = localStorage.getObj("language");
                }
                else {
                    language = "en";
                }
            },

            getLanguage: function () {
                this.loadLanguage();
                return language;
            },

            $get: function () {
                return {
                    saveLanguage: function () {
                        localStorage.setObj("language", language);
                    },
                    setLanguage: function (newLanguage) {
                        language = newLanguage;
                        this.saveLanguage();
                    }
                };
            }
        };
    });


    //service for fetching data from google public api for books
    libraryAppServices.factory(ServiceNames.GoogleAPI,
      [ServiceNames.$resource,
      function ($resource) {
          return $resource("https://www.googleapis.com/books/v1/volumes",
            {
                printType: "books",
                key: "AIzaSyCnaTjNhrH__aJ_ub4Eh7s9MaW3mFWrd7E"
            }, {
                query: {
                    method: "GET",
                    params: {
                        q: "",
                        maxResults: "20",
                        startIndex: "0"
                    },
                    isArray: true,
                    headers: {
                        "Content-Type": "application/json",
                        token: sessionStorage.getItem('token')
                    },
                    data: "",
                    dataType: "json",
                }
            });
      }]);


    //service for adding new book
    libraryAppServices.factory(ServiceNames.NewBookService,
      [ServiceNames.$resource,
      function ($resource) {

          return $resource(jsonStubUrl + "addNewBook", {},
             {
                 sendBook: {
                     method: "POST",
                     params: "",
                     isArray: false,
                     headers: {
                         "Content-Type": "application/json",
                         'JsonStub-User-Key': 'c439a796-888c-4ffc-9753-e31381ca18d4',
                         'JsonStub-Project-Key': '8f7dfc47-cb2a-4e93-b8ff-51c9101b009a',
                         token: sessionStorage.getItem('token'),
                         userId: sessionStorage.getObj("userId")
                     },
                     data: "",
                     dataType: "json",
                 }
             });
      }]);

    //service for adding new review to a book
    libraryAppServices.factory(ServiceNames.NewReviewService,
      [ServiceNames.$resource,
      function ($resource) {

          return $resource(jsonStubUrl + "addReview", {},
             {
                 sendReview: {
                     method: "PUT",
                     params: "",
                     isArray: false,
                     headers: {
                         "Content-Type": "application/json",
                         'JsonStub-User-Key': 'c439a796-888c-4ffc-9753-e31381ca18d4',
                         'JsonStub-Project-Key': '8f7dfc47-cb2a-4e93-b8ff-51c9101b009a',
                         token: sessionStorage.getItem('token'),
                         userId: sessionStorage.getItem("userId")
                     },
                     data: "",
                     dataType: "json",
                 }
             });
      }]);

    //update Timer for currently reading book
    libraryAppServices.factory(ServiceNames.UpdateTimer,
          [ServiceNames.$resource,
          function ($resource) {

              return $resource(jsonStubUrl + "updateTimer", {},
                 {
                     startStopTimer: {
                         method: "PUT",
                         params: "",
                         isArray: false,
                         headers: {
                             "Content-Type": "application/json",
                             'JsonStub-User-Key': 'c439a796-888c-4ffc-9753-e31381ca18d4',
                             'JsonStub-Project-Key': '8f7dfc47-cb2a-4e93-b8ff-51c9101b009a',
                             token: sessionStorage.getItem('token'),
                             userId: sessionStorage.getItem("userId")
                         },
                         data: "",
                         dataType: "json",
                     }
                 });
          }]);


    //update book status for a book from my library
    libraryAppServices.factory(ServiceNames.UpdateBookStatus,
          [ServiceNames.$resource,
          function ($resource) {

              return $resource(jsonStubUrl + "updateBookStatus", {},
                 {
                     update: {
                         method: "PUT",
                         params: "",
                         isArray: false,
                         headers: {
                             "Content-Type": "application/json",
                             'JsonStub-User-Key': 'c439a796-888c-4ffc-9753-e31381ca18d4',
                             'JsonStub-Project-Key': '8f7dfc47-cb2a-4e93-b8ff-51c9101b009a',
                             token: sessionStorage.getItem('token'),
                             userId: sessionStorage.getItem("userId")
                         },
                         data: "",
                         dataType: "json",
                     }
                 });
          }]);
})();